function listartipoVehiculo() {
    let peticion = sendRequest('tipovehiculo', 'GET', '');
    let tabla = document.getElementById("listadotipoVehiculo");
    ///Si la peticion Genera un status 200
    peticion.onload = function () {
        let datos = peticion.response;
        datos.forEach(tipovehiculo => {
            tabla.innerHTML += `
                <tr>
                    <td>${tipovehiculo.idTipoVehiculo}</td>
                    <td>${tipovehiculo.tipoVehiculo}</td>
                   
                    <td>
                        <img src='css/images/delete-24.png' onclick="eliminarTipoVehiculo(${tipovehiculo.idTipoVehiculo})" width="40px" alt="Eliminar TipoVehiculo"/>
                        <img src='css/images/edit_modify_icon-icons.com_72390 (1).png' onclick="editarTipoVehiculo('${tipovehiculo.idTipoVehiculo}')"
 width="40px" alt="Editar TipoVehiculo"/>

                    </td>
                </tr>
                `
        })
    }
    peticion.onerror = function () {
        Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: 'Error al Cargar los datos',
            showConfirmButton: false,
            timer: 1500
        })
    }
}
function mostrarFormulario() {
    let modaltipoVehiculo = new bootstrap.Modal(document.getElementById("modaltipoVehiculo"));
    modaltipoVehiculo.show();
    document.getElementById("btnGuardar").innerHTML = "Guardar";
}

function guardarTipoVehiculo() {
    let id = document.getElementById("txtId").value;
    let tipoVehiculo = document.getElementById("txttipoVehiculo").value;

    let datos = {};
    let peticion = "";
    let mensaje = "";
    if (id.length > 0) {
        datos = {
            "idTipoVehiculo": id,
            "tipoVehiculo": tipoVehiculo

        };
        peticion = sendRequest('tipovehiculo', 'PUT', datos);
        mensaje = "Editado Exitosamente";

    } else {
        datos = {
            "tipoVehiculo": tipoVehiculo
        };
        peticion = sendRequest('tipovehiculo', 'POST', datos);
        mensaje = "tipoVehiculo Registrado";


    }
    peticion.onload = function () {
        Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: mensaje,
            showConfirmButton: false,
            timer: 1500
        }).then((result) => {
            location.reload();
        })
    }


    peticion.onerror = function () {
        Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: 'Error al Cargar los datos',
            showConfirmButton: false,
            timer: 1500
        })
    }

    return false;
}

function eliminarTipoVehiculo(idTipoVehiculo) {
    Swal.fire({
        title: 'Esta seguro que desea Eliminar tipoVehiculo?',
        text: "Esta acción no se puede revertir!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, Eliminar'
    }).then((result) => {
        if (result.isConfirmed) {
            let peticion = sendRequest('tipovehiculo/' + idTipoVehiculo, 'DELETE', '');
            peticion.onload = function () {
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: 'tipoVehiculo Eliminado',
                    showConfirmButton: false,
                    timer: 1500
                }).then((result) => {
                    location.reload();
                })
            }
        }
    })



}
function editarTipoVehiculo(idTipoVehiculo) {
    let peticion = sendRequest('tipovehiculo/id?id=' + idTipoVehiculo, 'GET', '');
    peticion.onload = function () {
        let datos = peticion.response;
        console.log(datos);
        document.getElementById("txtId").value = datos.idTipoVehiculo;
        document.getElementById("txttipoVehiculo").value = datos.tipoVehiculo;

        mostrarFormulario();
        document.getElementById("btnGuardar").innerHTML = "Editar";

    }
}



