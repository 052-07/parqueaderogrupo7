/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.parqueaderoG07.parqueaderoG07.Controlador;

import com.parqueaderoG07.parqueaderoG07.Modelos.Plaza;
import com.parqueaderoG07.parqueaderoG07.Servicio.ServicioPlaza;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author juequ
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/plaza")
public class ControladorPlaza {

    @Autowired
    private ServicioPlaza servicio;

    

    @GetMapping("/lista")
    public List<Plaza> CosultarPlaza() {
        return servicio.findAll();
    }

    @GetMapping
    public List<Plaza> listarPlaza() {
        return servicio.listarPlaza();
    }

    @PostMapping
    public Plaza insertar(@RequestBody Plaza plaNuevo) {
        return servicio.insertar(plaNuevo);
    }

    @PutMapping
    public Plaza Actualizar(@RequestBody Plaza plaActualizar) {
        return servicio.insertar(plaActualizar);
    }

    @DeleteMapping(value = "/{idPlaza}")
    public ResponseEntity<Plaza> eliminar(@PathVariable Integer idPlaza) {
        Plaza objTipo = servicio.buscarPorId(idPlaza);
        try {
            servicio.eliminarPlaza(idPlaza);
            return new ResponseEntity<>(objTipo, HttpStatus.OK);

        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity<>(objTipo, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
