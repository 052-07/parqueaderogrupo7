/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.parqueaderoG07.parqueaderoG07.Controlador;

import com.parqueaderoG07.parqueaderoG07.Modelos.Registro;
import com.parqueaderoG07.parqueaderoG07.Servicio.ServicioRegistro;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author juequ
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/registro")
public class ControladorRegistro {

    @Autowired
    private ServicioRegistro servicio;

    @GetMapping
    public List<Registro> listarRegistro() {
        return servicio.listarRegistro();
    }

    @PostMapping
    public Registro insertar(@RequestBody Registro RegistroNuevo) {
        return servicio.insertar(RegistroNuevo);
    }

    @PutMapping
    public Registro Actualizar(@RequestBody Registro RegistroActualizar) {
        return servicio.insertar(RegistroActualizar);
    }

    @DeleteMapping(value = "/{idRegistro}")
    public ResponseEntity<Registro> eliminar(@PathVariable Integer idRegistro) {
        Registro objRegistro = servicio.buscarPorId(idRegistro);
        try {
            servicio.eliminarRegistro(idRegistro);
            return new ResponseEntity<>(objRegistro, HttpStatus.OK);

        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity<>(objRegistro, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
