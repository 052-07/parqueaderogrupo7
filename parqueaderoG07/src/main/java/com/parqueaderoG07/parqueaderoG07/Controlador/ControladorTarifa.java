/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.parqueaderoG07.parqueaderoG07.Controlador;

import com.parqueaderoG07.parqueaderoG07.Modelos.Tarifa;
import com.parqueaderoG07.parqueaderoG07.Modelos.TipoVehiculo;
import com.parqueaderoG07.parqueaderoG07.Servicio.ServicioTarifa;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author juequ
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/tarifa")
public class ControladorTarifa {
    
      @Autowired
    private ServicioTarifa servicio;
    
    @GetMapping
    public List<Tarifa> listar(){
        return servicio.listar();
    }
    
    @PostMapping
    public Tarifa insertar(@RequestBody Tarifa Nuevo){
        return servicio.insertar(Nuevo);
    }
    
    @PutMapping
    public Tarifa Actualizar(@RequestBody Tarifa Actualizar){
        return servicio.insertar(Actualizar);
    }

    @DeleteMapping(value = "/{idTarifa}")
    public ResponseEntity<Tarifa> eliminar(@PathVariable Integer idTarifa){
        Tarifa objTipo=servicio.buscarPorId(idTarifa);
        try {
            servicio.eliminarTarifa(idTarifa);
            return new ResponseEntity<>(objTipo,HttpStatus.OK);
            
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity<>(objTipo,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
}
