/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.parqueaderoG07.parqueaderoG07.Controlador;

import com.parqueaderoG07.parqueaderoG07.Modelos.TipoVehiculo;
import com.parqueaderoG07.parqueaderoG07.Servicio.ServicioTipoVehiculo;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author juequ
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/tipovehiculo")
public class ControladorTipoVehiculo {
    
    @Autowired
    private ServicioTipoVehiculo servicio;
    
    @GetMapping
    public List<TipoVehiculo> listarTipos(){
        return servicio.listarTipos();
    }
    
     @GetMapping("/id")
    public TipoVehiculo buscarporId( int id){
        return servicio.buscarPorId(id);
    }
    
    @PostMapping
    public TipoVehiculo insertar(@RequestBody TipoVehiculo tipoNuevo){
        return servicio.insertar(tipoNuevo);
    }
    
    @PutMapping
    public TipoVehiculo Actualizar(@RequestBody TipoVehiculo tipoActualizar){
        return servicio.insertar(tipoActualizar);
    }

    @DeleteMapping(value = "/{idTipoVehiculo}")
    public ResponseEntity<TipoVehiculo> eliminar(@PathVariable Integer idTipoVehiculo){
        TipoVehiculo objTipo=servicio.buscarPorId(idTipoVehiculo);
        try {
            servicio.eliminarTipoVehiculo(idTipoVehiculo);
            return new ResponseEntity<>(objTipo,HttpStatus.OK);
            
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity<>(objTipo,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    
}
