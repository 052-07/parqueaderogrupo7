/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.parqueaderoG07.parqueaderoG07.Controlador;

import com.parqueaderoG07.parqueaderoG07.Modelos.Usuario;

import com.parqueaderoG07.parqueaderoG07.Servicio.ServicioUsuario;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author crjlo
 */

@RestController
@CrossOrigin("*")
@RequestMapping("/usuario")
public class ControladorUsuario {
    
    @Autowired
    private ServicioUsuario servicio;
    
    @GetMapping
    public List<Usuario> listar(){
        return servicio.listar();
    }
    
    @PostMapping
    public Usuario insertar(@RequestBody Usuario Nuevo){
        return servicio.insertar(Nuevo);
    }
    
    @PutMapping
    public Usuario Actualizar(@RequestBody Usuario Actualizar){
        return servicio.insertar(Actualizar);
    }

    @DeleteMapping(value = "/{idUsuario}")
    public ResponseEntity<Usuario> eliminar(@PathVariable Integer idUsuario){
        Usuario objTipo=servicio.buscarPorId(idUsuario);
        try {
            servicio.eliminarUsuario(idUsuario);
            return new ResponseEntity<>(objTipo,HttpStatus.OK);
            
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity<>(objTipo,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
}
