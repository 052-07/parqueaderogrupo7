/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.parqueaderoG07.parqueaderoG07.Dao;

import com.parqueaderoG07.parqueaderoG07.Modelos.Parqueadero;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author juequ
 */
public interface DaoParqueadero extends JpaRepository<Parqueadero,Integer >{
    
}
