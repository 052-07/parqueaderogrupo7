package com.parqueaderoG07.parqueaderoG07;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ParqueaderoG07Application {

	public static void main(String[] args) {
		SpringApplication.run(ParqueaderoG07Application.class, args);
	}

}
