/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.parqueaderoG07.parqueaderoG07.Servicio;

import com.parqueaderoG07.parqueaderoG07.Dao.DaoParqueadero;
import com.parqueaderoG07.parqueaderoG07.Modelos.Parqueadero;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author juequ
 */
@Service
public class ServicioParqueadero {
    
    @Autowired
    private DaoParqueadero daoParqueadero;
    
    public Parqueadero insertar (Parqueadero ParqueaderoNuevo){
        return daoParqueadero.save(ParqueaderoNuevo);
    }
    
    public List<Parqueadero> listarTipos(){
        return daoParqueadero.findAll();
    }
    
    public Parqueadero buscarPorId(int idParqueadero){
        return daoParqueadero.findById(idParqueadero).get().orElse(null);
    }
    
    /**
     *
     * @param idParqueadero
     */
    public void eliminarParqueadero(int idParqueadero){
        daoParqueadero.delete(daoParqueadero.findById(idParqueadero).get());
    }

    public void eliminarparqueadero(int idParqueadero){
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    
}
