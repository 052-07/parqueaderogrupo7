/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.parqueaderoG07.parqueaderoG07.Servicio;

import com.parqueaderoG07.parqueaderoG07.Dao.DaoPlaza;
import com.parqueaderoG07.parqueaderoG07.Modelos.Plaza;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author juequ
 */
@Service
public class ServicioPlaza {

    @Autowired
    private DaoPlaza daoPlaza;

    public Plaza insertar(Plaza PlazaNuevo) {
        return daoPlaza.save(PlazaNuevo);
    }

    public List<Plaza> listarPlaza() {
        return daoPlaza.findAll();
    }

    public Plaza buscarPorId(int idPlaza) {
        return daoPlaza.findById(idPlaza).get().orElse(null);
    }

    public void eliminarPlaza(int idPlaza) {
        daoPlaza.delete(daoPlaza.findById(idPlaza).get());
    }

    public List<Plaza> findAll() {
        return (List<Plaza>) daoPlaza.findAll();
    }

}
