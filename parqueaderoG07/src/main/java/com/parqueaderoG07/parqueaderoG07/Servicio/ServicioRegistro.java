/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.parqueaderoG07.parqueaderoG07.Servicio;

import com.parqueaderoG07.parqueaderoG07.Dao.DaoRegistro;
import com.parqueaderoG07.parqueaderoG07.Modelos.Registro;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author juequ
 */
@Service
public class ServicioRegistro {

    @Autowired
    private DaoRegistro daoRegistro;

    public Registro insertar(Registro RegistroNuevo) {
        return daoRegistro.save(RegistroNuevo);
    }

    public List<Registro> listar() {
        return daoRegistro.findAll();
    }

    public Registro buscarPorId(int idRegistro) {
        return daoRegistro.findById(idRegistro).get().orElse(null);
    }

    public void eliminarRegistro(int idRegistro) {
        daoRegistro.delete(daoRegistro.findById(idRegistro).get());
    }

    public List<Registro> listarRegistro() {
        return daoRegistro.findAll();
    }
}
