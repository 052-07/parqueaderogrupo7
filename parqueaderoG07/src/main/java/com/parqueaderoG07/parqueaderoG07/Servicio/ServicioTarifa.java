/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.parqueaderoG07.parqueaderoG07.Servicio;

import com.parqueaderoG07.parqueaderoG07.Dao.DaoTarifa;
import com.parqueaderoG07.parqueaderoG07.Modelos.Tarifa;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author juequ
 */
@Service
public class ServicioTarifa {
    
    @Autowired
    private DaoTarifa dao;
    
     public Tarifa insertar (Tarifa Nuevo){
        return dao.save(Nuevo);
    }
    
    public List<Tarifa> listartarifa(){
        return dao.findAll();
    }
    
    public Tarifa buscarPorId(int idTarifa){
        return dao.findById(idTarifa).get().orElse(null);
    }
    
    public void eliminarTarifa(int idTarifa){
        dao.delete(dao.findById(idTarifa).get());
    }

    public List<Tarifa> listar() {
       return dao.findAll();
    }
    
}
