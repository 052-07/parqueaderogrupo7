/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.parqueaderoG07.parqueaderoG07.Servicio;

import com.parqueaderoG07.parqueaderoG07.Dao.DaoTipoVehiculo;
import com.parqueaderoG07.parqueaderoG07.Modelos.TipoVehiculo;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author juequ
 */
@Service
public class ServicioTipoVehiculo { 
    
    @Autowired
    private DaoTipoVehiculo daoTipoVehiculo;
    
    public TipoVehiculo insertar (TipoVehiculo tipoVehiculoNuevo){
        return daoTipoVehiculo.save(tipoVehiculoNuevo);
    }
    
    public List<TipoVehiculo> listarTipos(){
        return daoTipoVehiculo.findAll();
    }
    
    public TipoVehiculo buscarPorId(int idTipoVehiculo){
        return daoTipoVehiculo.findById(idTipoVehiculo).get();
    }
    
    public void eliminarTipoVehiculo(int idTipoVehiculo){
        daoTipoVehiculo.delete(daoTipoVehiculo.findById(idTipoVehiculo).get());
    }

  

    
   
    
}
