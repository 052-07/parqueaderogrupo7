/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.parqueaderoG07.parqueaderoG07.Servicio;

import com.parqueaderoG07.parqueaderoG07.Dao.DaoUsuario;
import com.parqueaderoG07.parqueaderoG07.Modelos.Usuario;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author crjlo
 */
@Service
public class ServicioUsuario {

    @Autowired
    private DaoUsuario dao;

    public Usuario insertar(Usuario Nuevo) {
        return dao.save(Nuevo);
    }

    public List<Usuario> listarUsuarios() {
        return dao.findAll();
    }

    public Usuario buscarPorId(int idUsuario) {
//        return dao.findById(idUsuario).get().orElse(null);
        return dao.findById(idUsuario).get();
    }

    public void eliminarUsuario(int idUsuario) {
        dao.delete(dao.findById(idUsuario).get());
    }

    public List<Usuario> listar() {
        return dao.findAll();
    }

}
